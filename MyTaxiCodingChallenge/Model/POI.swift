//
//  POI.swift
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON

class POI: NSObject {
    var id: Int?
    var coordinate: CLLocationCoordinate2D?
    var state: String?
    var type: String?
    var heading: CGFloat?
    
    // MARK: - Instance methods
    
    override init() {
        self.id = -1
        self.coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
        self.state = ""
        self.type = ""
        self.heading = 0.0
    }
    
    init(id: Int, latitude: Double, longitude: Double, state: String, type: String, heading: CGFloat) {
        self.id = id
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.state = state
        self.type = type
        self.heading = heading
    }
    
    init(json: JSON) {
        self.id = json["id"].int
        if let latitude = json["coordinate"]["latitude"].double, let longitude = json["coordinate"]["longitude"].double {
            self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
        self.state = json["state"].string
        self.type = json["type"].string
        if let heading = json["heading"].double {
            self.heading = CGFloat(heading)
        }
    }
}
