//
//  String.swift
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

import UIKit

extension String {
    // MARK: - Instance methods
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
