//
//  UIImage+Rotation.h
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Rotation)

-(UIImage*)imageRotatedByDegrees:(CGFloat)degrees;

@end
