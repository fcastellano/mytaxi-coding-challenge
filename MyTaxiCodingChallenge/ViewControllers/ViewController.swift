//
//  ViewController.swift
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Toast_Swift

class ViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var poiArray: [POI]?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //MARK: - Private
    
    func setupView() {
        self.mapView.register(TaxiAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        self.mapView.delegate = self
        self.goTo(coordinates: Constants.kHamburgCoordinate)
        
        self.searchBar.delegate = self
        self.searchBar.placeholder = "searchbar_placeholder".localized()
    }
    
    func goTo(coordinates: CLLocationCoordinate2D) {
        let camera = MKMapCamera(lookingAtCenter: coordinates, fromDistance: 2000, pitch: 0, heading: 0)
        self.mapView.setCamera(camera, animated: true)
    }
    
    func getPoi(topLeftCoordinate: CLLocationCoordinate2D, bottomRightCoordinate: CLLocationCoordinate2D) {
        POIManager.sharedInstance.getPOI(topLeftCoordinate: topLeftCoordinate, bottomRightCoordinate: bottomRightCoordinate) { (poiArray, serviceError) in
            if serviceError != nil || poiArray.count == 0 {
                self.view.makeToast("taxi_error_message".localized())
            }
            self.poiArray = poiArray
            if let poiArray = self.poiArray {
                POIManager.sharedInstance.addAnnotationsToMap(mapView: self.mapView, poiArray: poiArray)
            }
        }
    }
    
    func convertAdressToCoordinates(address: String) {
        CLGeocoder().geocodeAddressString(address) { (placemarks, error) in
            if let placemarks = placemarks, let placemark = placemarks.first, let location = placemark.location {
                self.goTo(coordinates: location.coordinate)
            }
        }
    }
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.getPoi(topLeftCoordinate: mapView.topLeftCoordinate(), bottomRightCoordinate: mapView.bottomRightCoordinate())
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? TaxiAnnotation else {
            return nil
        }
        
        let identifier = Constants.kAnnotationId
        var view: TaxiAnnotationView
        if let taxiView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? TaxiAnnotationView {
            taxiView.annotation = annotation
            view = taxiView
        } else {
            view = TaxiAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.image = annotation.customImage
        }
        return view
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            self.convertAdressToCoordinates(address: text)
        }
        searchBar.resignFirstResponder()
    }
}
