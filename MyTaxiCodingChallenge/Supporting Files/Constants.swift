//
//  Constants.swift
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

import UIKit
import CoreLocation

@objcMembers
class Constants: NSObject {
    static let kHamburgCoordinate = CLLocationCoordinate2D(latitude: 53.55, longitude: 9.99)
    static let kPOIURL = "https://poi-api.mytaxi.com/PoiService/poi/v1"
    static let kAnnotationId = "TaxiAnnotationId"
    public static let kTaxiStateActive = "ACTIVE"
    public static let kTaxiStateActiveImageName = "Taxi"
    public static let kTaxiStateInactiveImageName = "TaxiInactive"
    
    //MARK: - Tests
    
    static let kPOIFailURL = "https://poi-api.mytaxi.com/PoiService/pois/v1"
    
    static let kTestSucceedCoordinateTopLeft = CLLocationCoordinate2D(latitude: 53.694865, longitude: 9.757589)
    static let kTestSucceedCoordinateBottomRight = CLLocationCoordinate2D(latitude: 53.394655, longitude: 10.099891)
    static let kTestFailCoordinateTopLeft = CLLocationCoordinate2D(latitude: 40, longitude: 40)
    static let kTestFailCoordinateBottomRight = CLLocationCoordinate2D(latitude: 39, longitude: 41)
    
    static let kTestFalseCoordinateDistanceTopLeft = CLLocationCoordinate2D(latitude: 53.694865, longitude: 9.757589)
    static let kTestFalseCoordinateDistanceBottomRight = CLLocationCoordinate2D(latitude: 53.694865, longitude: 9.757589)
    static let kTestTrueCoordinateDistanceTopLeft = CLLocationCoordinate2D(latitude: 53.694865, longitude: 9.757589)
    static let kTestTrueCoordinateDistanceBottomRight = CLLocationCoordinate2D(latitude: 53.394655, longitude: 10.099891)
}
