//
//  TaxiAnnotationView.m
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

#import "TaxiAnnotationView.h"
#import "TaxiAnnotation.h"

@implementation TaxiAnnotationView
// MARK: - Instance methods

- (void)setAnnotation:(id<MKAnnotation>)annotation {
    [super setAnnotation:annotation];
    if ([annotation isKindOfClass:TaxiAnnotation.class]) {
        TaxiAnnotation *taxiAnnotation = (TaxiAnnotation *)annotation;
        self.canShowCallout = true;
        self.calloutOffset = CGPointMake(-5, 5);
        self.image = taxiAnnotation.customImage;
    }
}

@end
