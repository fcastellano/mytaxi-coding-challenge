//
//  TaxiAnnotation.h
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface TaxiAnnotation : NSObject<MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) int taxiAnnotationId;
@property (nonatomic) CGFloat heading;
@property (nonatomic, strong) UIImage *customImage;

-(instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate TaxiAnnotationId:(int)taxiAnnotationId Heading:(CGFloat)heading Type:(NSString *)type ImageName:(NSString *)imageName;

@end
