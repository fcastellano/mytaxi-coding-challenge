//
//  TaxiAnnotation.m
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

#import "TaxiAnnotation.h"
#import "UIImage+Rotation.h"

@implementation TaxiAnnotation
@synthesize title, coordinate, taxiAnnotationId, heading, customImage;

-(instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate TaxiAnnotationId:(int)taxiAnnotationId Heading:(CGFloat)heading Type:(NSString *)type ImageName:(NSString *)imageName {
    self = [super init];
    if (!self ) {
        return nil;
    }
    self.taxiAnnotationId = taxiAnnotationId;
    self.heading = heading;
    self.title = [NSString stringWithFormat:@"%@ #%d", type, taxiAnnotationId];
    self.coordinate = coordinate;
    self.customImage = [[UIImage imageNamed:imageName] imageRotatedByDegrees:heading];
    return self;
}

@end
