//
//  TaxiAnnotationView.h
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface TaxiAnnotationView : MKAnnotationView

@end
