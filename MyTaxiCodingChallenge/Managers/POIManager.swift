//
//  POIManager.swift
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import MapKit

class POIManager {
    static let sharedInstance = POIManager()
    
    // MARK: - Instance methods
    
    func getPOI(topLeftCoordinate: CLLocationCoordinate2D, bottomRightCoordinate: CLLocationCoordinate2D, completion: ((_ response: [POI], _ error: ServiceError?) -> Void)? = nil) {
        let parameters = [
            "p1Lon":topLeftCoordinate.longitude,
            "p1Lat":topLeftCoordinate.latitude,
            "p2Lon":bottomRightCoordinate.longitude,
            "p2Lat":bottomRightCoordinate.latitude
        ]
        
        ServiceManager.sharedInstance.useService(url: Constants.kPOIURL, method: .get, parameters: parameters) { (json, serviceError) in
            if let completion = completion, let json = json, let array = json["poiList"].array {
                var poiArray = [POI]()
                for jsonItem in array {
                    poiArray.append(POI(json: jsonItem))
                }
                completion(poiArray, serviceError)
            }
        }
    }
    
    func addAnnotationsToMap(mapView: MKMapView, poiArray: [POI]) {
        for poi in poiArray {
            if let coordinate = poi.coordinate, let id = poi.id, let heading = poi.heading, let type = poi.type, let state = poi.state {
                let taxiAnnotation = TaxiAnnotation(coordinate: coordinate, taxiAnnotationId: Int32(id), heading: heading, type: type, imageName: state == Constants.kTaxiStateActive ? Constants.kTaxiStateActiveImageName : Constants.kTaxiStateInactiveImageName)
                if !self.checkIfAnnotationIsInPlace(mapView: mapView, id: id, newCoordinate: coordinate) {
                    self.removeAnnotationForId(mapView: mapView, id: id)
                    mapView.addAnnotation(taxiAnnotation!)
                }
            }
        }
    }
    
    func checkIfAnnotationIsInPlace(mapView: MKMapView, id: Int, newCoordinate: CLLocationCoordinate2D) -> Bool {
        if let annotation = getAnnotationForId(mapView: mapView, id: id) {
            return CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude).distance(from: CLLocation(latitude: newCoordinate.latitude, longitude: newCoordinate.longitude)) > 10
        }
        return false
    }
    
    func getAnnotationForId(mapView: MKMapView, id: Int) -> MKAnnotation? {
        return mapView.annotations.filter { (annotation) -> Bool in
            if let annotation = annotation as? TaxiAnnotation, annotation.taxiAnnotationId == id {
                return true
            }
            return false
        }.first
    }
    
    func removeAnnotationForId(mapView: MKMapView, id: Int) {
        if let annotation = self.getAnnotationForId(mapView: mapView, id: id) {
            mapView.removeAnnotation(annotation)
        }
    }
}
