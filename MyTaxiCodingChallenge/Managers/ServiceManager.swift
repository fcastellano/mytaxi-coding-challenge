//
//  ServiceManager.swift
//  MyTaxiCodingChallenge
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ServiceManager {
    static let sharedInstance = ServiceManager()
    
    // MARK: - Instance methods
    
    func useService(url: String, method: HTTPMethod, parameters: Parameters?, completion: ((_ response: JSON?, _ error: ServiceError?) -> Void)? = nil) {
        Alamofire.request(url, method: method, parameters: parameters).responseJSON { response in
            if let completion = completion {
                if let responseServer = response.result.value, let code = response.response?.statusCode {
                    if code == 200 {
                        completion(JSON(responseServer), nil)
                    } else {
                        completion(nil, ServiceError(errorCode: code, urlRequest: url))
                    }
                } else {
                    if let code = response.response?.statusCode {
                        completion(nil, ServiceError(errorCode: code, urlRequest: url))
                    }
                }
            }
        }
    }
}
