//
//  POIManagerTests.swift
//  MyTaxiCodingChallengeTests
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

import XCTest
import MapKit

class POIManagerTests: XCTestCase {
    let poiManager = POIManager.sharedInstance
    var mapView = MKMapView(frame: CGRect.zero)
    var poi = POI(id: -1, latitude: Constants.kHamburgCoordinate.latitude, longitude: Constants.kHamburgCoordinate.longitude, state: "ACTIVE", type: "TAXI", heading: 0)
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetPOISucceed() {
        var poiArray = [POI]()
        let expectation = self.expectation(description: "AsyncWait")
        self.poiManager.getPOI(topLeftCoordinate: Constants.kTestSucceedCoordinateTopLeft, bottomRightCoordinate: Constants.kTestSucceedCoordinateBottomRight) { (poi, serviceError) in
            poiArray = poi
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssert(poiArray.count > 0)
    }
    
    func testGetPOIFail() {
        var poiArray = [POI]()
        let expectation = self.expectation(description: "AsyncWait")
        self.poiManager.getPOI(topLeftCoordinate: Constants.kTestFailCoordinateTopLeft, bottomRightCoordinate: Constants.kTestFailCoordinateBottomRight) { (poi, serviceError) in
            poiArray = poi
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssert(poiArray.count == 0)
    }
    
    func testAddAnnotationsToMap() {
        self.addAnnotation()
        XCTAssert(self.mapView.annotations.count == 1)
    }
    
    func testRemoveAnnotationsToMap() {
        self.addAnnotation()
        XCTAssert(self.mapView.annotations.count == 1)
        self.poiManager.removeAnnotationForId(mapView: self.mapView, id: -1)
        XCTAssert(self.mapView.annotations.count == 0)
    }
    
    func testGetAnnotationByIdSucceded() {
        self.addAnnotation()
        let annotation = self.poiManager.getAnnotationForId(mapView: self.mapView, id: -1)
        XCTAssert(annotation != nil)
    }
    
    func testGetAnnotationByIdFail() {
        let annotation = self.poiManager.getAnnotationForId(mapView: self.mapView, id: -1)
        XCTAssert(annotation == nil)
    }
    
    func testCheckDistanceFalse() {
        self.poi.coordinate = Constants.kTestFalseCoordinateDistanceTopLeft
        self.addAnnotation()
        XCTAssert(!self.poiManager.checkIfAnnotationIsInPlace(mapView: self.mapView, id: -1, newCoordinate: Constants.kTestFalseCoordinateDistanceBottomRight))
    }
    
    func testCheckDistanceTrue() {
        self.poi.coordinate = Constants.kTestTrueCoordinateDistanceTopLeft
        self.addAnnotation()
        XCTAssert(self.poiManager.checkIfAnnotationIsInPlace(mapView: self.mapView, id: -1, newCoordinate: Constants.kTestTrueCoordinateDistanceBottomRight))
    }
    
    func addAnnotation() {
        let poiArray = [poi]
        self.poiManager.addAnnotationsToMap(mapView: self.mapView, poiArray: poiArray)
    }
}
