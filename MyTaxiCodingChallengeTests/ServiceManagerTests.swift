//
//  ServiceManagerTests.swift
//  MyTaxiCodingChallengeTests
//
//  Created by Franco Castellano on 7/26/18.
//  Copyright © 2018 Castellano. All rights reserved.
//

import XCTest

class ServiceManagerTests: XCTestCase {
    let poiManager = ServiceManager.sharedInstance
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testWebServiceFail() {
        var serviceErrorTest: ServiceError?
        let expectation = self.expectation(description: "AsyncWait")
        self.poiManager.useService(url: Constants.kPOIFailURL, method: .get, parameters: nil) { (json, serviceError) in
            serviceErrorTest = serviceError
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssert(serviceErrorTest != nil)
    }
}
